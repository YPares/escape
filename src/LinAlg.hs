module LinAlg where

import qualified Data.Vector.V2 as V2


type V2 = V2.Vector2

v2 = V2.Vector2
