{-# LANGUAGE TypeFamilies, ConstraintKinds, MultiParamTypeClasses, FlexibleInstances, PolyKinds, TypeOperators, DataKinds, FlexibleContexts, GADTs, ScopedTypeVariables, Rank2Types #-}
{-# LANGUAGE UndecidableInstances #-}

module TypeLevel where

import Control.Applicative
import qualified Data.Data as D
import Data.Foldable (foldrM)
import Data.Hashable
import qualified Data.IntMap.Lazy as I
import Data.List (intersperse)
import Data.Vinyl
import Data.Vinyl.TypeLevel
import GHC.TypeLits
import GHC.Exts (Constraint)

import qualified Control.Lens as L -- To remove after


type family RSub (a :: [k]) (el :: k) where
  RSub '[]       el = '[]
  RSub (a ': as) a = as
  RSub (b ': as) el = b ': RSub as el

type family RSetSub (a :: [k]) (b :: [k]) where
  RSetSub a '[]       = a
  RSetSub a (b ': bs) = RSetSub (RSub a b) bs

-- | set c is set a minus set b
type IRSetSub c a b = (c ⊆ a, c ~ RSetSub a b, a ⊆ (b ++ RSetSub a b))

--instance (IRSetSub cs as bs, img ~ (RImage cs as))
--         => RSubset cs as img


castFrom :: (b ⊆ a) => Rec f b -> Rec f a -> Rec f b
castFrom _ x = rcast x

-- | Tag erasure
data UT f where
  UT :: (Show (f a), D.Typeable (f a)) => f a -> UT f
instance Show (UT Module) where
  show (UT m) = show m

newtype DynRec f = DynRec (I.IntMap (UT f))
instance (Show (UT f)) => Show (DynRec f) where
  show (DynRec m) =
    "<DynRec>{" ++ concat (intersperse ", " $ map (show . snd)
                                                  (I.toList m))
                ++ "}"

eraseTags :: (RecAll f rs Show
             ,RecAll f rs D.Typeable)
          => Rec f rs -> [UT f]
eraseTags RNil = []
eraseTags (m :& rs) = UT m : eraseTags rs

castUT :: (D.Typeable (f r)) => UT f -> Maybe (f r)
castUT (UT x) = D.cast x

class (RecAll f rs D.Typeable)
   => TypeableRec f rs where
  retrieveTags :: [UT f] -> Maybe (Rec f rs)
instance TypeableRec f '[] where
  retrieveTags [] = Just RNil
  retrieveTags _  = Nothing
instance (TypeableRec f rs, D.Typeable (f r))
      => TypeableRec f (r ': rs) where
  retrieveTags [] = Nothing
  retrieveTags ((UT ut):uts) = do x <- D.cast ut
                                  xs <- retrieveTags uts
                                  return (x :& xs)

findInUTs :: (D.Typeable (f r)) => [UT f] -> Maybe (f r)
findInUTs = foldr (\x r -> castUT x <|> r) empty

{-class (RecAll f rs D.Typeable)
      => FlexibleCast f rs where
  flexibleCast :: [UT f] -> Maybe (Rec f rs)
instance FlexibleCast f '[] where
  flexibleCast _ = Just RNil
instance (FlexibleCast f rs, D.Typeable (f r))
         => FlexibleCast f (r ': rs) where
  flexibleCast [] = Nothing
  flexibleCast uts = do x <- (findInUTs uts :: Maybe (f r))
                        xs <- (flexibleCast uts :: Maybe (Rec f rs))
                        return (x :& xs)
-}

-- | A custom proxy type so we don't conflict with
-- potential instances of Hashable for Data.Proxy.Proxy
data HashProxy n = HashProxy

instance (KnownNat n) => Hashable (HashProxy n) where
  hashWithSalt s = hashWithSalt s . natVal

instance (KnownSymbol s) => Hashable (HashProxy s) where
  hashWithSalt s = hashWithSalt s . symbolVal
  
class (RecAll HashProxy rs Hashable
      , RecAll f rs D.Typeable)
      => FromDynRec f rs where
  toDynRec :: Rec f rs -> DynRec f
  fromDynRec :: DynRec f -> Maybe (Rec f rs)
instance FromDynRec f '[] where
  toDynRec _ = DynRec I.empty
  fromDynRec _ = Just RNil
instance (Hashable (HashProxy r), D.Typeable (f r), Show (f r)
         , FromDynRec f rs)
         => FromDynRec f (r ': rs) where
  toDynRec (r :& rs) = DynRec $ I.insert (hash (HashProxy :: HashProxy r))
                                         (UT r)
                                         intMap
    where (DynRec intMap) = toDynRec rs
  fromDynRec d@(DynRec intMap) = do
    (UT dr) <- I.lookup (hash (HashProxy :: HashProxy r)) intMap
    r <- D.cast dr  -- Could be replaced by unsafeCoerce
    rs <- fromDynRec d
    return (r :& rs)

fromDynRec' :: (FromDynRec f rs) => Rec f rs -> DynRec f -> Maybe (Rec f rs)
fromDynRec' _ = fromDynRec

rlensDyn :: forall proxy f r.
         (Hashable (HashProxy r), D.Typeable f, D.Typeable r
         , Show (f r))
         => proxy r -> L.Simple L.Lens (DynRec f) (Maybe (f r))
rlensDyn _ = onDR . L.at (hash (HashProxy :: HashProxy r)) . onUT
  where onDR f (DynRec intMap) = fmap DynRec (f intMap)
        onUT f (Just (UT v)) = fmap (fmap UT) (f (D.cast v))
        onUT f Nothing       = fmap (fmap UT) (f Nothing)

data SplitRec f rs = SplitRec { _staticPart :: Rec f rs
                              , _dynamicPart :: DynRec f }
                   deriving (Show)

staticPart f (SplitRec s d) = fmap (\s' -> SplitRec s' d) (f s)
dynamicPart f (SplitRec s d) = fmap (\d' -> SplitRec s d') (f d)

type family Inside (r :: k) (rs :: [k]) where
  Inside r (r ': rs) = True
  Inside r '[]       = False
  Inside r (u ': rs) = Inside r rs

class (contains ~ Inside r rs)
      => SplitLensH (f :: k -> *) (rs :: [k]) (r :: k)
                    (contains :: Bool) where
  type SplitLensInnerType f r contains
  slens :: proxy r -> L.Simple L.Lens (SplitRec f rs)
                                      (SplitLensInnerType f r contains)
instance (r ∈ rs, Inside r rs ~ True) => SplitLensH f rs r True where
  type SplitLensInnerType f r True = f r
  slens p = staticPart . rlens p
instance (D.Typeable f, D.Typeable r, Hashable (HashProxy r)
         , Inside r rs ~ False
         , Show (f r))
         => SplitLensH f rs r False where
  type SplitLensInnerType f r False = Maybe (f r)
  slens p = dynamicPart . rlensDyn p

class (sub ~ RSetSub (a ': as) rs)
  => SplitLensH2 (f :: k -> *) (rs :: [k]) (a :: k) (as :: [k]) (sub :: [k])
  where
    type OneFieldInnerType f rs a as sub
    type SeveralFieldsInnerType f rs a as sub
    slens2 :: proxy a -> L.Simple L.Lens (SplitRec f rs)
                                     (OneFieldInnerType f rs a as sub)
    ssubset :: proxy asked -> L.Simple L.Lens (SplitRec f rs)
                            (SeveralFieldsInnerType f rs a as sub)

-- No asked field is missing
instance (a ∈ rs, as ⊆ rs, RSetSub (a ': as) rs ~ '[])
  => SplitLensH2 f rs a as '[]
  where
    type OneFieldInnerType f rs a as '[] = f a
    type SeveralFieldsInnerType f rs a as '[] = Rec f (a ': as)
    slens2 p = staticPart . rlens p
    ssubset p = staticPart . rsubset

-- Some fields are missing (m ': ms) from the asked fields
instance (D.Typeable f, D.Typeable a, D.Typeable as
         , RecAll HashProxy (a ': as) Hashable
         , (m ': ms) ~ RSetSub (a ': as) rs
         , RecAll f (a ': as) Show)
         => SplitLensH2 f rs a as (m ': ms)
  where
    type OneFieldInnerType f rs a as (m ': ms) =
      Maybe (f a)
    type SeveralFieldsInnerType f rs a as (m ': ms) =
      Maybe (Rec f (a ': as))
    slens2 p = dynamicPart . rlensDyn p
    ssubset p f (SplitRec s d) = undefined
      --let wantedRec =
      --in fmap (\) f wantedRec
    
type SplitLens f rs r = SplitLensH f rs r (Inside r rs)

splitCast :: forall f as bs cs.
             (IRSetSub cs as bs, bs ⊆ as, FromDynRec f cs)
          => Rec f as -> SplitRec f bs
splitCast x = SplitRec (rcast x) (toDynRec (rcast x :: Rec f cs))

splitCastFrom :: (IRSetSub cs as bs, bs ⊆ as, FromDynRec f cs)
              => proxy bs -> Rec f as -> SplitRec f bs
splitCastFrom _ x = splitCast x

mergeCast :: forall f as bs cs.
             (FromDynRec f as, FromDynRec f bs, FromDynRec f cs
             , IRSetSub cs as bs)
          => SplitRec f bs -> Maybe (Rec f as)
mergeCast (SplitRec s d) = do
  d' <- fromDynRec d :: Maybe (Rec f cs)
  return (rcast (s <+> d'))

mergeCastFrom :: (FromDynRec f as, FromDynRec f bs, FromDynRec f (RSetSub as bs)
                 , as ⊆ (bs ++ RSetSub as bs)
                 , (RSetSub as bs) ⊆ as)
              => proxy as -> SplitRec f bs -> Maybe (Rec f as)
mergeCastFrom _ = mergeCast

rsubsetFrom :: (Functor g, RSubset rs ss (RImage rs ss))
            => sing rs -> (Rec f rs -> g (Rec f rs)) -> Rec f ss -> g (Rec f ss)
rsubsetFrom _ = rsubset



class (Show (Module tag))
  => IModule (tag :: k) where
  data Module tag

  type Deps tag :: [k]
  type Deps tag = '[]

type family SatisfiedDepsH
     (tags :: [k])
     (alreadyVerified :: [k]) :: Constraint where
  SatisfiedDepsH '[] vs = ()
  SatisfiedDepsH (r ': rs) vs = (Deps r ⊆ (rs ++ vs)
                                ,SatisfiedDepsH rs (r ': vs))
  
type SatisfiedDeps rs = SatisfiedDepsH rs '[]

checkDeps :: (SatisfiedDeps rs) => Rec f rs -> ()
checkDeps _ = ()
