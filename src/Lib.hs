module Lib
    ( someFunc
    ) where

import Graphics.Triangulation.Delaunay
import qualified Data.Vector.V2 as V2

someFunc :: IO ()
someFunc = putStrLn "someFunc"
