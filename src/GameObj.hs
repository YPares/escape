{-# LANGUAGE CPP, TypeOperators, FlexibleContexts, GADTs, TypeFamilies, FlexibleInstances, DataKinds, PolyKinds #-}

module GameObj where

import LinAlg
import TypeLevel

import Control.Lens
import Data.Vinyl
import Data.Vinyl.TypeLevel
import GHC.TypeLits

(=::) :: KnownSymbol s => proxy '(s,a) -> a -> ElField '(s,a)
(=::) _ x = Field x

{-
flens :: (Functor g, '(s, a) ∈ rs) =>
         sing '(s, a) -> (a -> g a) -> FieldRec rs -> g (FieldRec rs)
flens field = rlens field . rfield

foo :: Proxy '("foo", String)
foo = Proxy

bar :: Proxy '("bar", Int)
bar = Proxy

rec1 :: FieldRec ['("foo", String), '("bar", Int)]
rec1 = foo =:: "Hello"
    :& bar =:: 2
    :& RNil

getFoo :: ('("foo", String) ∈ fields) => FieldRec fields -> String
getFoo = view (flens foo)

--#define FIELD(name, type)\
  name :: Proxy '("name", type); name = Proxy

FIELD(location, V2)
FIELD(horizSpeed, V2)
FIELD(vertSpeed, V2)
FIELD(solidity, Double)
FIELD(perception, Double)
-}

-- | Variant of rlens with no proxy
flens
  :: (Functor g, r ∈ rs) =>
     (f r -> g (f r)) -> Rec f rs -> g (Rec f rs)
flens = rlens undefined

class ProxyModule a where
  type ModuleTag a :: Symbol
  makeProxyModule :: a -> Module (ModuleTag a)
instance ProxyModule (Module (t::Symbol)) where
  type ModuleTag (Module t) = t
  makeProxyModule x = x
instance (ProxyModule b) => ProxyModule (a -> b) where
  type ModuleTag (a -> b) = ModuleTag b
  makeProxyModule f = makeProxyModule (f undefined)

mlens
  :: (Functor g, (ModuleTag a) ∈ rs, ProxyModule a) =>
     a -> (f (ModuleTag a) -> g (f (ModuleTag a))) -> Rec f rs -> g (Rec f rs)
mlens m = rlens (makeProxyModule m)

mlensDyn m = rlensDyn (makeProxyModule m)

mslens m = slens (makeProxyModule m)

newtype GameObjId = GameObjId Int
  deriving (Show)

type GameObj rs = Rec Module rs

instance IModule "MId" where
  data Module "MId" = MId GameObjId
                    deriving (Show)
instance IModule "Location" where
  data Module "Location" = Location V2
                         deriving (Show)
instance IModule "HMovement" where
  data Module "HMovement" = HMovement V2
                          deriving (Show)
  type Deps "HMovement" = '["Location"]
instance IModule "VMovement" where
  data Module "VMovement" = VMovement V2
                          deriving (Show)
  type Deps "VMovement" = '["Location"]
instance IModule "Solidity" where
  data Module "Solidity" = Solidity Double
                         deriving (Show)
instance IModule "Perception" where
  data Module "Perception" = Perception Double
                           deriving (Show)
instance IModule "Master" where
  data Module "Master" = Master (GameObj '["MId"])
                       deriving (Show)

-- | Get a lens to the inside of the module
class OneValueModule (s::Symbol) where
  type ModuleValue s
  mvalue :: Lens' (Module s) (ModuleValue s)
#define OVM_INST(name, valtype)\
  instance OneValueModule "name" where\
  { type ModuleValue "name" = valtype;\
    mvalue f (name s) = fmap name (f s); }

mlensv l = mlens l . mvalue
mlensvDyn l = mlensDyn l . _Just . mvalue

OVM_INST(MId, GameObjId)
OVM_INST(Location, V2)
OVM_INST(HMovement, V2)
OVM_INST(VMovement, V2)
OVM_INST(Solidity, Double)
OVM_INST(Perception, Double)
type ObjWithIdentity = GameObj '["MId"]
  -- ^ Can't use directly type lists in the macro call below
OVM_INST(Master, ObjWithIdentity)


master = Location (v2 1 1) :& MId (GameObjId 0) :& RNil
robot = MId (GameObjId 1)
     :& Location (v2 0 0)
     :& HMovement (v2 0 0)
     :& Solidity 2
     :& Master (view rsubset master)
     :& RNil

{-
r2 :: GameObj '["MId", "HMovement", "Location", "Solidity", "Master"]
--r2 = over rsubset (\(HMovement x :& RNil) -> HMovement x :& RNil) robot
r2 = over (mlens HMovement) (\(HMovement x) -> HMovement x) robot
-}
